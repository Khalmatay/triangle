def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a <= 0 or b <= 0 or 0 >= c:
        return False
    
    if (a + b > c ) & (a + c > b) & (b + c > a):
        return True

    return False
